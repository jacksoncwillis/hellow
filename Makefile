CFLAGS = -Wall -Wextra -Wpedantic
DEBUG_CFLAGS = -g -O0
RELEASE_CFLAGS = -O2

LIBS = -lreadline

DIST_DIR = dist

DEBUG_DIR = $(DIST_DIR)/debug
RELEASE_DIR = $(DIST_DIR)/release

DEBUG_CC = $(CC) $(CFLAGS) $(DEBUG_CFLAGS)
RELEASE_CC = $(CC) $(CFLAGS) $(RELEASE_CFLAGS)

################################################################################

hello.c: hello.h

$(DEBUG_DIR)/hello: hello.c
	$(DEBUG_CC) $(LIBS) hello.c -o $(DEBUG_DIR)/hello

$(RELEASE_DIR)/hello: hello.c
	$(RELEASE_CC) $(LIBS) hello.c -o $(RELEASE_DIR)/hello

################################################################################

$(DEBUG_DIR)/prompt: prompt.c
	$(DEBUG_CC) prompt.c $(LIBS) -o $(DEBUG_DIR)/prompt

$(RELEASE_DIR)/prompt: prompt.c
	$(RELEASE_CC) prompt.c $(LIBS) -o $(RELEASE_DIR)/prompt

################################################################################

.PHONY: all clean remake prepare_debug prepare_release debug release
all: release
clean:
	rm -rf $(DIST_DIR)
remake: clean all
prepare_debug:
	@mkdir -p $(DEBUG_DIR)
prepare_release:
	@mkdir -p $(RELEASE_DIR)
debug: prepare_debug
release: prepare_release

debug: $(DEBUG_DIR)/hello
debug: $(DEBUG_DIR)/prompt
release: $(RELEASE_DIR)/hello
release: $(RELEASE_DIR)/prompt
