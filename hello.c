#include <stdio.h>

#include "hello.h"

int
main(int argc, char * argv[]) {
  if (argc < 2) {
    puts("You have no arguments.");
  } else if (argc == 2) {
    printf("You have one argument: \"%s\".\n", argv[1]);
  } else if (argc == 3) {
    printf("You have two arguments: \"%s\" and \"%s\".\n", argv[1],
      argv[2]);
  } else {
    printf("You have many arguments:");
    for (int i = 1; i < argc - 1; i++) {
      printf(" \"%s\",", argv[i]);
    }
    printf(" and \"%s\".\n", argv[argc - 1]);
  }

  char * state_names[] = {
    "Wisconsin",
    "Michigan",
    "Minnesota",
    "Iowa",
    "Illinois"
  };

  int state_founding_dates[] = {
    1848,
    1837,
    1858,
    1846,
    1818
  };

  int num_states = (int)ARRAY_SIZE(state_names);
  printf("Number of states: %d.\n", num_states);

  for (int i = 0; i < num_states; i++) {
    char * state_name = state_names[i];
    int founding_date = state_founding_dates[i];
    printf("State #%d: %s, founded %d.\n", i + 1, state_name, founding_date);
  }

  char * seasons[] = {
    "Spring",
    "Summer",
    "Autumn",
    "Winter"
  };

  char ** first_season = seasons;
  for (int i = 0; i < (int)ARRAY_SIZE(seasons); i++) {
    char * season = * (first_season + i);
    printf("Season #%d: %s.\n", i + 1, season);
  }

  return 0;
}
