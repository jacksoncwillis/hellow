#include <stdio.h>
#include <stdlib.h>

#include <readline/readline.h>
#include <readline/history.h>

int main() {
  puts("Lispy version 0.0.1");

  while (1) {
    char *input = readline("> ");
    add_history(input);
    printf("# %s\n", input);
    free(input);
  }
}
